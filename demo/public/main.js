(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _yeluguri_entertainments_yeluguri_entertainments_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./yeluguri-entertainments/yeluguri-entertainments.component */ "./src/app/yeluguri-entertainments/yeluguri-entertainments.component.ts");
/* harmony import */ var _yeluguri_weddings_yeluguri_weddings_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./yeluguri-weddings/yeluguri-weddings.component */ "./src/app/yeluguri-weddings/yeluguri-weddings.component.ts");
/* harmony import */ var _yeluguri_albums_yeluguri_albums_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./yeluguri-albums/yeluguri-albums.component */ "./src/app/yeluguri-albums/yeluguri-albums.component.ts");
/* harmony import */ var _yeluguri_aboutus_yeluguri_aboutus_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./yeluguri-aboutus/yeluguri-aboutus.component */ "./src/app/yeluguri-aboutus/yeluguri-aboutus.component.ts");
/* harmony import */ var _yeluguri_rentastudio_yeluguri_rentastudio_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./yeluguri-rentastudio/yeluguri-rentastudio.component */ "./src/app/yeluguri-rentastudio/yeluguri-rentastudio.component.ts");
/* harmony import */ var _yeluguri_bookings_yeluguri_bookings_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./yeluguri-bookings/yeluguri-bookings.component */ "./src/app/yeluguri-bookings/yeluguri-bookings.component.ts");
/* harmony import */ var _yeluguri_careers_yeluguri_careers_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./yeluguri-careers/yeluguri-careers.component */ "./src/app/yeluguri-careers/yeluguri-careers.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    { path: '', redirectTo: '/yeluguri-entertainments', pathMatch: 'full' },
    {
        path: 'yeluguri-entertainments',
        component: _yeluguri_entertainments_yeluguri_entertainments_component__WEBPACK_IMPORTED_MODULE_2__["YeluguriEntertainmentsComponent"]
    },
    {
        path: 'yeluguri-bookings',
        component: _yeluguri_bookings_yeluguri_bookings_component__WEBPACK_IMPORTED_MODULE_7__["YeluguriBookingsComponent"]
    },
    {
        path: 'yeluguri-weddings',
        component: _yeluguri_weddings_yeluguri_weddings_component__WEBPACK_IMPORTED_MODULE_3__["YeluguriWeddingsComponent"]
    },
    {
        path: 'yeluguri-albums',
        component: _yeluguri_albums_yeluguri_albums_component__WEBPACK_IMPORTED_MODULE_4__["YeluguriAlbumsComponent"]
    },
    {
        path: 'yeluguri-aboutus',
        component: _yeluguri_aboutus_yeluguri_aboutus_component__WEBPACK_IMPORTED_MODULE_5__["YeluguriAboutusComponent"]
    },
    {
        path: 'yeluguri-careers',
        component: _yeluguri_careers_yeluguri_careers_component__WEBPACK_IMPORTED_MODULE_8__["YeluguriCareersComponent"]
    },
    {
        path: 'yeluguri-rentastudio',
        component: _yeluguri_rentastudio_yeluguri_rentastudio_component__WEBPACK_IMPORTED_MODULE_6__["YeluguriRentastudioComponent"]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\" style='background:#000;'>\n\n    <!-- Navbar Start here -->\n    <div class=\"container\">\n      <mdb-navbar SideClass=\"navbar navbar-expand-lg navbar-dark\" style='background:#000;'>\n          <mdb-navbar-brand *ngIf=\"homePageData.logo!=''\"><a class=\"navbar-brand\" routerLink=\"/yeluguri-entertainments\"><img style=\"height:40px;\" src=\"{{homePageData.logo}}\" alt=\"Wedding Photographers – Yeluguri Entertainments\" class=\"logo-img img-pc\"></a></mdb-navbar-brand>\n          <links>\n              <ul class=\"navbar-nav ml-auto\">\n                  <li class=\"nav-item\" [routerLinkActive]=\"['cactive']\">\n                      <a class=\"nav-link waves-light\" mdbWavesEffect  routerLink=\"/yeluguri-entertainments\">\n                        <i class=\"fa fa-home\"></i>\n                      </a>\n                  </li>\n                  <li class=\"nav-item\"  [routerLinkActive]=\"['cactive']\">\n                      <a class=\"nav-link waves-light\" mdbWavesEffect  routerLink=\"/yeluguri-weddings\">WEDDINGS</a>\n                  </li>\n                  <li class=\"nav-item\" [routerLinkActive]=\"['cactive']\">\n                      <a class=\"nav-link waves-light\" mdbWavesEffect  routerLink=\"/yeluguri-albums\">ALBUMS</a>\n                  </li>\n                  <li class=\"nav-item\" [routerLinkActive]=\"['cactive']\">\n                      <a class=\"nav-link waves-light\" mdbWavesEffect  routerLink=\"/yeluguri-aboutus\">ABOUT</a>\n                  </li>\n                  <li class=\"nav-item\" [routerLinkActive]=\"['cactive']\">\n                      <a class=\"nav-link waves-light\" mdbWavesEffect  routerLink=\"/yeluguri-bookings\">BOOK EVENT</a>\n                  </li>\n                  <li class=\"nav-item\" [routerLinkActive]=\"['cactive']\">\n                      <a class=\"nav-link waves-light\" mdbWavesEffect  routerLink=\"/yeluguri-careers\">JOIN US</a>\n                  </li>\n                  <li class=\"nav-item\" *ngIf=\"homePageData.facebook!=''\">\n                      <a class=\"btn-floating purple-gradient waves-light header-btns c-btns\" mdbWavesEffect  target=\"_blank\" href=\"{{homePageData.facebook}}\">\n                        <i class=\"fa fa-facebook fa-2x\"></i>\n                      </a>\n                  </li>\n                  <li class=\"nav-item\" *ngIf=\"homePageData.instagram!=''\">\n                      <a class=\"btn-floating peach-gradient waves-light header-btns c-btns\" mdbWavesEffect target=\"_blank\" href=\"{{homePageData.instagram}}\">\n                          <i class=\"fa fa-instagram fa-2x\" aria-hidden=\"true\"></i>\n                      </a>\n                  </li>\n                  <li class=\"nav-item\" *ngIf=\"homePageData.youtube!=''\">\n                      <a class=\"btn-floating blue-gradient waves-light header-btns c-btns\" mdbWavesEffect target=\"_blank\" href=\"{{homePageData.youtube}}\">\n                        <i class=\"fa fa-youtube-play fa-2x\"></i>\n                      </a>\n                  </li>\n                  <li class=\"nav-item\"  *ngIf=\"homePageData.phone!=''\">\n                      <button type=\"button\" class=\"btn btn-info btn-md header-btns phone-number\" mdbWavesEffect>\n                        <i class=\"fa fa-whatsapp\" aria-hidden=\"true\" style=\"font-size: 1.6rem;\"></i> \n                        {{homePageData.phone}}\n                      </button>\n                  </li>\n              </ul>\n          </links>\n      </mdb-navbar>\n    </div>\n    <!-- Navbar End here -->\n</div>\n\n<div class=\"container-fluid\">\n    <router-outlet></router-outlet>\n</div>\n\n<div class=\"container\">\n<div class=\"row\" style='margin-top:60px;margin-bottom:40px;'>\n    <div class=\"col-sm-12 col-md-4\">\n        <div class=\"card card-cascade\" style='height:375px;'>\n            <div class=\"view view-cascade  gradient-card-header purple-gradient\">\n                <h4 class=\"card-header-title footer-card\">BOOK US NOW</h4>\n            </div>\n\n            <div class=\"card-body card-body-cascade text-center\">\n                <p class=\"card-text\" style='height:200px'  innerHTML=\"{{homePageData.bookus}}\">\n                </p>\n                <p>\n                    <button (click)=\"book()\" class=\"btn purple-gradient btn-rounded btn-sm waves-light\" mdbWavesEffect>Book us now</button>\n                </p>\n              </div>\n        </div>\n    </div>\n\n    <div class=\"col-sm-12 col-md-4\">\n        <div class=\"card card-cascade\" style='height:375px;'>\n            <div class=\"view view-cascade gradient-card-header peach-gradient\">\n                <h4 class=\"card-header-title footer-card\">Join Our Team</h4>\n            </div>\n            <div class=\"card-body card-body-cascade text-center\">\n                <p class=\"card-text\" style='height:200px'   innerHTML=\"{{homePageData.joinus}}\">\n                    \n                </p>\n                <p>\n                    <button (click)=\"join()\" class=\"btn peach-gradient btn-rounded btn-sm waves-light\" mdbWavesEffect>Join us now</button>\n                </p>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"col-sm-12 col-md-4\">\n        <div class=\"card card-cascade\" style='height:375px;'>\n            <div class=\"view view-cascade gradient-card-header blue-gradient\">\n                <h4 class=\"card-header-title footer-card\">Rent a Studio</h4>\n            </div>\n            <div class=\"card-body card-body-cascade text-center\">\n                <p class=\"card-text\" style='height:200px'   innerHTML=\"{{homePageData.rentastudio}}\">\n                       \n                </p>\n                <p>\n                    <button (click)=\"book()\" class=\"btn blue-gradient btn-sm btn-rounded waves-light\" mdbWavesEffect>Rent a studio</button>\n                </p>\n            </div>\n        </div>\n    </div>\n</div>\n</div>\n<div  class=\"container-fluid\" style='margin-top:60px;background:#000;color:#fff;padding:30px;'>\n    <div class=\"container\">\n            {{homePageData.copyright}}\n    </div>\n   \n</div>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(afs) {
        var _this = this;
        this.afs = afs;
        this.homePageData = { copyright: '', sliders: [], facebook: '', instagram: '', youtube: '', logo: '', phone: '', teasers: [], email: '', about: { img: '', details: '' }, bookus: '', joinus: '', rentastudio: '' };
        this.itemsCollection = afs.collection('info');
        this.itemDoc = afs.doc('info/NtRkKdFFCIXYCnvMHO78');
        this.itemsd = this.itemsCollection.valueChanges();
        this.itemsd.subscribe(function (data) {
            _this.homePageData = data[0];
        });
    }
    AppComponent.prototype.book = function () {
        window.location.href = '/yeluguri-bookings';
    };
    AppComponent.prototype.join = function () {
        window.location.href = "/yeluguri-careers";
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var ng_pick_datetime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng-pick-datetime */ "./node_modules/ng-pick-datetime/picker.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _yeluguri_entertainments_yeluguri_entertainments_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./yeluguri-entertainments/yeluguri-entertainments.component */ "./src/app/yeluguri-entertainments/yeluguri-entertainments.component.ts");
/* harmony import */ var _yeluguri_weddings_yeluguri_weddings_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./yeluguri-weddings/yeluguri-weddings.component */ "./src/app/yeluguri-weddings/yeluguri-weddings.component.ts");
/* harmony import */ var _yeluguri_albums_yeluguri_albums_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./yeluguri-albums/yeluguri-albums.component */ "./src/app/yeluguri-albums/yeluguri-albums.component.ts");
/* harmony import */ var _yeluguri_aboutus_yeluguri_aboutus_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./yeluguri-aboutus/yeluguri-aboutus.component */ "./src/app/yeluguri-aboutus/yeluguri-aboutus.component.ts");
/* harmony import */ var _yeluguri_rentastudio_yeluguri_rentastudio_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./yeluguri-rentastudio/yeluguri-rentastudio.component */ "./src/app/yeluguri-rentastudio/yeluguri-rentastudio.component.ts");
/* harmony import */ var _yeluguri_bookings_yeluguri_bookings_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./yeluguri-bookings/yeluguri-bookings.component */ "./src/app/yeluguri-bookings/yeluguri-bookings.component.ts");
/* harmony import */ var _yeluguri_careers_yeluguri_careers_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./yeluguri-careers/yeluguri-careers.component */ "./src/app/yeluguri-careers/yeluguri-careers.component.ts");
/* harmony import */ var angularfire2__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! angularfire2 */ "./node_modules/angularfire2/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! angularfire2/storage */ "./node_modules/angularfire2/storage/index.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
/* harmony import */ var ng_uikit_pro_standard__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ng-uikit-pro-standard */ "./node_modules/ng-uikit-pro-standard/esm5/ng-uikit-pro-standard.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _yeluguri_entertainments_yeluguri_entertainments_component__WEBPACK_IMPORTED_MODULE_7__["YeluguriEntertainmentsComponent"],
                _yeluguri_weddings_yeluguri_weddings_component__WEBPACK_IMPORTED_MODULE_8__["YeluguriWeddingsComponent"],
                _yeluguri_albums_yeluguri_albums_component__WEBPACK_IMPORTED_MODULE_9__["YeluguriAlbumsComponent"],
                _yeluguri_aboutus_yeluguri_aboutus_component__WEBPACK_IMPORTED_MODULE_10__["YeluguriAboutusComponent"],
                _yeluguri_rentastudio_yeluguri_rentastudio_component__WEBPACK_IMPORTED_MODULE_11__["YeluguriRentastudioComponent"],
                _yeluguri_bookings_yeluguri_bookings_component__WEBPACK_IMPORTED_MODULE_12__["YeluguriBookingsComponent"],
                _yeluguri_careers_yeluguri_careers_component__WEBPACK_IMPORTED_MODULE_13__["YeluguriCareersComponent"]
            ],
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                ng_uikit_pro_standard__WEBPACK_IMPORTED_MODULE_19__["MDBBootstrapModulesPro"].forRoot(),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                ng_pick_datetime__WEBPACK_IMPORTED_MODULE_4__["OwlDateTimeModule"],
                ng_pick_datetime__WEBPACK_IMPORTED_MODULE_4__["OwlNativeDateTimeModule"],
                angularfire2__WEBPACK_IMPORTED_MODULE_14__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_15__["environment"].firebase),
                angularfire2_firestore__WEBPACK_IMPORTED_MODULE_16__["AngularFirestoreModule"],
                angularfire2_auth__WEBPACK_IMPORTED_MODULE_18__["AngularFireAuthModule"],
                angularfire2_storage__WEBPACK_IMPORTED_MODULE_17__["AngularFireStorageModule"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"].withServerTransition({ appId: 'serverApp' }),
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"]
            ],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["NO_ERRORS_SCHEMA"]],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/yeluguri-aboutus/yeluguri-aboutus.component.html":
/*!******************************************************************!*\
  !*** ./src/app/yeluguri-aboutus/yeluguri-aboutus.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row text-center\" style=\"margin-top:30px;margin-bottom:8px;\">\n        <h1 style=\"width:100%;border-bottom: 5px solid #ff7043;padding-bottom: 8px;\">ABOUT US</h1>\n    </div>\n    <div class=\"row\" style=\"margin-bottom:40px; text-align:justify;\" innerHTML=\"{{page.about}}\">\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/yeluguri-aboutus/yeluguri-aboutus.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/yeluguri-aboutus/yeluguri-aboutus.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/yeluguri-aboutus/yeluguri-aboutus.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/yeluguri-aboutus/yeluguri-aboutus.component.ts ***!
  \****************************************************************/
/*! exports provided: YeluguriAboutusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YeluguriAboutusComponent", function() { return YeluguriAboutusComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var YeluguriAboutusComponent = /** @class */ (function () {
    function YeluguriAboutusComponent(afs) {
        var _this = this;
        this.afs = afs;
        this.page = { about: '', book: '', album: '', wedding: '' };
        this.itemsCollectionw = afs.collection('page');
        this.itemDocw = afs.doc('page/EI9bBobft8w6GCLL3s2L');
        this.itemsdw = this.itemsCollectionw.valueChanges();
        this.itemsdw.subscribe(function (data) {
            _this.page = data[0];
        });
    }
    YeluguriAboutusComponent.prototype.ngOnInit = function () {
    };
    YeluguriAboutusComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-yeluguri-aboutus',
            template: __webpack_require__(/*! ./yeluguri-aboutus.component.html */ "./src/app/yeluguri-aboutus/yeluguri-aboutus.component.html"),
            styles: [__webpack_require__(/*! ./yeluguri-aboutus.component.scss */ "./src/app/yeluguri-aboutus/yeluguri-aboutus.component.scss")]
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]])
    ], YeluguriAboutusComponent);
    return YeluguriAboutusComponent;
}());



/***/ }),

/***/ "./src/app/yeluguri-albums/yeluguri-albums.component.html":
/*!****************************************************************!*\
  !*** ./src/app/yeluguri-albums/yeluguri-albums.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <div class=\"container\">\n    <div class=\"row text-center\" style=\"margin-top:30px;margin-bottom:8px;\">\n        <h1 style=\"width:100%;border-bottom: 5px solid #ff7043;padding-bottom: 8px;\">ALBUMS</h1>\n    </div>\n    <div class=\"row\" style=\"margin-bottom:40px;\" class=\"wedding-text\" innerHTML=\"{{page.album}}\">\n      <blockquote>\n      </blockquote>\n    </div>\n    <!-- BRIDAL PORTRAITS -->\n    <div *ngFor=\"let item of albumData.albums;\">\n      <div class=\"row text-center\" style=\"margin-top:30px;\">\n          <h2 style=\"width:100%;font-size: 30px;\">\n            {{item.name}}\n          </h2>\n      </div>\n      <div class=\"row\">\n        <div class='col-sm-12 slider' style='height:600px'>\n          <a *ngFor=\"let albumimg of item.albumImages;let i = index;\" href=\"{{albumimg}}\" data-lightbox=\"{{item.name}}\">\n            <img src=\"{{albumimg}}\" class=\"album-img\"/>\n          </a>\n        </div>\n      </div>\n      <hr/>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/yeluguri-albums/yeluguri-albums.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/yeluguri-albums/yeluguri-albums.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/yeluguri-albums/yeluguri-albums.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/yeluguri-albums/yeluguri-albums.component.ts ***!
  \**************************************************************/
/*! exports provided: YeluguriAlbumsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YeluguriAlbumsComponent", function() { return YeluguriAlbumsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import 'slick-carousel';

var YeluguriAlbumsComponent = /** @class */ (function () {
    function YeluguriAlbumsComponent(afs) {
        var _this = this;
        this.afs = afs;
        this.page = { about: '', book: '', album: '', wedding: '' };
        this.weddingData = { weddings: [] };
        this.albumData = { albums: [] };
        this.itemsCollectionw = afs.collection('page');
        this.itemDocw = afs.doc('page/EI9bBobft8w6GCLL3s2L');
        this.itemsdw = this.itemsCollectionw.valueChanges();
        this.itemsdw.subscribe(function (data) {
            _this.page = data[0];
        });
        this.itemsCollection = afs.collection('albums');
        this.itemDoc = afs.doc('albums/IBsOvBKsVE9SA8lnyq4i');
        this.itemsd = this.itemsCollection.valueChanges();
        this.itemsd.subscribe(function (data) {
            _this.albumData = data[0];
            console.log(_this.albumData);
            setTimeout(function () {
                jquery__WEBPACK_IMPORTED_MODULE_1__(".slider").slick({
                    autoplay: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 1,
                    arrows: true,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 4000,
                            settings: {
                                slidesToShow: 1
                            }
                        },
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 1
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                arrows: true,
                                slidesToShow: 1
                            }
                        }
                    ]
                });
            }, 3000);
        });
    }
    YeluguriAlbumsComponent.prototype.ngOnInit = function () {
        jquery__WEBPACK_IMPORTED_MODULE_1__(document).ready(function () {
        });
    };
    YeluguriAlbumsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-yeluguri-albums',
            template: __webpack_require__(/*! ./yeluguri-albums.component.html */ "./src/app/yeluguri-albums/yeluguri-albums.component.html"),
            styles: [__webpack_require__(/*! ./yeluguri-albums.component.scss */ "./src/app/yeluguri-albums/yeluguri-albums.component.scss")]
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"]])
    ], YeluguriAlbumsComponent);
    return YeluguriAlbumsComponent;
}());



/***/ }),

/***/ "./src/app/yeluguri-bookings/yeluguri-bookings.component.html":
/*!********************************************************************!*\
  !*** ./src/app/yeluguri-bookings/yeluguri-bookings.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <div class=\"row text-center\" style=\"margin-top:30px;margin-bottom:8px;\">\n            <h1 style=\"width:100%;border-bottom: 5px solid #ff7043;padding-bottom: 8px;\">BOOK OUR SERVICES</h1>\n        </div>\n        <div class=\"row\" style=\"margin-bottom:40px;\" class=\"wedding-text\" innerHTML=\"{{page.book}}\">\n                <blockquote>\n                        \n                </blockquote>\n        </div>\n  <div class=\"md-form\">\n      <input mdbInputDirective type=\"text\" id=\"form1\" class=\"form-control\" [(ngModel)]=\"eventData.name\">\n      <label for=\"form1\" class=\"\">Full Name</label>\n    </div>\n    <br/>\n    <div class=\"md-form\">\n      <input mdbInputDirective type=\"text\" id=\"form1\" class=\"form-control\"  [(ngModel)]=\"eventData.email\">\n      <label for=\"form1\" class=\"\">Email</label>\n    </div>\n    <br/>\n    <div class=\"md-form\">\n      <input mdbInputDirective type=\"text\" id=\"form1\" class=\"form-control\"  [(ngModel)]=\"eventData.phone\">\n      <label for=\"form1\" class=\"\">Phone Number</label>\n    </div>\n    <br/>\n    <div class=\"md-form\">\n      <input mdbInputDirective type=\"text\" id=\"form1\" class=\"form-control\"  [(ngModel)]=\"eventData.city\">\n      <label for=\"form1\" class=\"\">City / Street</label>\n    </div>\n    <br/>\n    <div class=\"md-form\">\n        <textarea type=\"text\" id=\"form7\" class=\"md-textarea form-control\" mdbInputDirective [(ngModel)]=\"eventData.more\"></textarea>\n        <label for=\"form7\">Tell us more about the event</label>\n    </div>\n    <br/>\n    <div class=\"row\">\n        <div class=\"col-sm-12 col-md-12\">     \n            <div class=\"btn-group\" mdbDropdown>\n                <label style=\"padding-right:15px;margin-top:10px;\">Select Event:</label>\n                <button mdbDropdownToggle type=\"button\" class=\"btn btn-info dropdown-toggle waves-light\" mdbWavesEffect> {{eventData.mainEvent}} </button>\n                <div class=\"dropdown-menu dropdown-primary\">\n                    <a class=\"dropdown-item\"  *ngFor=\"let item of eventDatay.events; let j = index\" (click)=\"mainEventSelected(item)\">{{item.mainEventName}}</a>\n                </div>\n            </div>\n        </div>\n    </div>\n    <br/><br/>\n    <div class=\"row\">\n        <div class=\"col-sm-12 col-md-12\">            \n            <label style=\"padding-right:15px;margin-top:10px;\">Event Details:</label><button type=\"button\" class=\"btn btn-info btn-md waves-light\" mdbWavesEffect (click)=\"addEvent()\">ADD EVENT</button>\n        </div>\n    </div>\n    \n    <div class=\"row\">\n        <div class=\"card\" *ngFor=\"let item of eventOptions; let i = index\">\n          <div class=\"card-body\">\n                <div class=\"btn-group\" mdbDropdown>\n                    <button mdbDropdownToggle type=\"button\" class=\"btn btn-primary dropdown-toggle waves-light\" mdbWavesEffect> {{item.eventType}} </button>\n                    <div class=\"dropdown-menu dropdown-primary\">\n                        <a class=\"dropdown-item\" *ngFor=\"let eventType of subEvents; let j = index\" (click)=\"eventSelected(item,eventType)\">{{eventType}}</a>\n                    </div>\n                </div>\n                <br/><br/>\n                <input [owlDateTime]=\"dt1\" type=\"text\" [(ngModel)]=\"item.eventDate\"  [min]=\"item.eventDate\" [owlDateTimeTrigger]=\"dt1\" placeholder=\"Date Time\">\n                <owl-date-time #dt1></owl-date-time>\n                <br/>\n                <div class=\"md-form\">\n                    <input mdbInputDirective type=\"text\" id=\"form1\" class=\"form-control\" [(ngModel)]=\"item.eventDetails\">\n                    <label for=\"form1\" class=\"\">Details</label>\n                </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\" style=\"margin-top:30px;margin-bottom:8px;\">\n          <div class=\"col-lg-12 text-right\">\n            <button type=\"button\" class=\"btn btn-warning waves-light\" mdbWavesEffect (click)=\"submitBooking()\">Submit</button>\n          </div>\n      </div>\n\n      <div mdbModal #content=\"mdbModal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBasicModalLabel\"aria-hidden=\"true\">\n            <div class=\"modal-dialog\" role=\"document\">\n                <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                    <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"content.hide()\">\n                    <span aria-hidden=\"true\">×</span>\n                    </button>\n                    <h4 class=\"modal-title w-100\" id=\"myModalLabel\">ERRORS</h4>\n                </div>\n                <div class=\"modal-body\">\n                    Please fill all the items in the form to book event\n                </div>\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-secondary waves-light\" aria-label=\"Close\" (click)=\"content.hide()\" mdbWavesEffect>Close</button>\n                </div>\n                </div>\n            </div>\n            </div>\n\n</div>"

/***/ }),

/***/ "./src/app/yeluguri-bookings/yeluguri-bookings.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/yeluguri-bookings/yeluguri-bookings.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/yeluguri-bookings/yeluguri-bookings.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/yeluguri-bookings/yeluguri-bookings.component.ts ***!
  \******************************************************************/
/*! exports provided: YeluguriBookingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YeluguriBookingsComponent", function() { return YeluguriBookingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var YeluguriBookingsComponent = /** @class */ (function () {
    function YeluguriBookingsComponent(afs) {
        var _this = this;
        this.afs = afs;
        this.eventDatay = { events: [] };
        this.eventData = { name: '', email: '', phone: '', city: '', more: '', mainEvent: 'Select Event', eventData: [] };
        this.page = { about: '', book: '', album: '', wedding: '' };
        this.itemsCollectionw = afs.collection('page');
        this.itemDocw = afs.doc('page/EI9bBobft8w6GCLL3s2L');
        this.itemsdw = this.itemsCollectionw.valueChanges();
        this.itemsdw.subscribe(function (data) {
            _this.page = data[0];
        });
        this.itemsCollection = afs.collection('events');
        this.itemDoc = afs.doc('events/AKyy6XfUCjsmyJphGUeg');
        this.itemsd = this.itemsCollection.valueChanges();
        this.itemsd.subscribe(function (data) {
            _this.eventDatay = data[0];
            console.log(data[0]);
        });
    }
    YeluguriBookingsComponent.prototype.ngOnInit = function () {
        this.eventOptions = [];
    };
    YeluguriBookingsComponent.prototype.addEvent = function () {
        this.eventOptions.push({ eventType: 'Select Event', eventDate: new Date(), eventDetails: '' });
    };
    YeluguriBookingsComponent.prototype.mainEventSelected = function (item) {
        this.eventOptions = [];
        this.eventData.mainEvent = item.mainEventName;
        this.subEvents = item.subEvents;
    };
    YeluguriBookingsComponent.prototype.eventSelected = function (item, event) {
        item.eventType = event;
    };
    YeluguriBookingsComponent.prototype.submitBooking = function () {
        this.eventData.eventData = this.eventOptions;
        if (this.eventData.name != '' && this.eventData.email != '' && this.eventData.phone != '' && this.eventData.city != '' && this.eventData.mainEvent != 'Select Event' && this.eventData.eventData.length > 0) {
        }
        else {
            this.contentModal.show();
        }
        console.log(this.eventData);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('content'),
        __metadata("design:type", Object)
    ], YeluguriBookingsComponent.prototype, "contentModal", void 0);
    YeluguriBookingsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-yeluguri-bookings',
            template: __webpack_require__(/*! ./yeluguri-bookings.component.html */ "./src/app/yeluguri-bookings/yeluguri-bookings.component.html"),
            styles: [__webpack_require__(/*! ./yeluguri-bookings.component.scss */ "./src/app/yeluguri-bookings/yeluguri-bookings.component.scss")]
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]])
    ], YeluguriBookingsComponent);
    return YeluguriBookingsComponent;
}());



/***/ }),

/***/ "./src/app/yeluguri-careers/yeluguri-careers.component.html":
/*!******************************************************************!*\
  !*** ./src/app/yeluguri-careers/yeluguri-careers.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row text-center\" style=\"margin-top:30px;margin-bottom:8px;\">\n        <h1 style=\"width:100%;border-bottom: 5px solid #ff7043;padding-bottom: 8px;\">GROW WITH US</h1>\n    </div>\n    <div class=\"row\" style=\"margin-bottom:40px;\" class=\"wedding-text\">\n            <blockquote>\n                Together with new input from your side, work with us on developing something one of a kind for clients,that literally blow their minds.You can learn and hone your skills while you work with us. Opportunities where freedom of expression is given, free rein come by only once in a lifetime. Utilise it as you work on our team and figure out your true potential.\n            </blockquote>\n    </div>\n    <h2 style=\"text-align: center;margin-bottom: 15px;\">Please Fill The Form</h2>\n    <form class=\"form-inline1\" action=\"\">\n      <div class=\"form-group row\">\n        <label for=\"name\" class=\"col-lg-2 text-right\">Name:</label>\n        <input type=\"text\" class=\"form-control  col-lg-9 col-lg-9\" id=\"name\" placeholder=\"Enter Full Name\" name=\"name\">\n        <span class=\"col-lg-1\"></span>\n      </div>\n      <div class=\"form-group row\">\n        <label for=\"email\"  class=\"col-lg-2 text-right\">Email:</label>\n        <input type=\"email\" class=\"form-control  col-lg-9\" id=\"email\" placeholder=\"Enter email\" name=\"email\">\n        <span class=\"col-lg-1\"></span>\n      </div>\n      <div class=\"form-group row\">\n        <label for=\"pwd\" class=\"col-lg-2 text-right\">Phone:</label>\n        <input type=\"text\" class=\"form-control  col-lg-9\" id=\"pwd\" placeholder=\"Enter Phone Number\" name=\"phone\">\n        <span class=\"col-lg-1\"></span>\n      </div>\n      <div class=\"form-group row\">\n        <label for=\"cafrom\" class=\"col-lg-2 text-right\">City Applying From:</label>\n        <input type=\"text\" class=\"form-control  col-lg-9\" id=\"cafrom\" placeholder=\"City Applying From\" name=\"cafrom\">\n        <span class=\"col-lg-1\"></span>\n      </div>\n      <div class=\"form-group row\">\n        <label for=\"dafor\" class=\"col-lg-2 text-right\">Department applying for:</label>\n        <input type=\"text\" class=\"form-control  col-lg-9\" id=\"dafor\" placeholder=\"Department applying for\" name=\"dafor\">\n        <span class=\"col-lg-1\"></span>\n      </div>\n      <div class=\"form-group row\">\n        <label for=\"ctype\" class=\"col-lg-2 text-right\">Candidate type:</label>\n        <input type=\"text\" class=\"form-control  col-lg-9\" id=\"ctype\" placeholder=\"Candidate type\" name=\"ctype\">\n        <span class=\"col-lg-1\"></span>\n      </div>\n      <div class=\"form-group row\">\n        <label for=\"keyskills\" class=\"col-lg-2 text-right\">Key Skills:</label>\n        <input type=\"text\" class=\"form-control  col-lg-9\" id=\"keyskills\" placeholder=\"Key Skills\" name=\"keyskills\">\n        <span class=\"col-lg-1\"></span>\n      </div>\n      <div class=\"form-group row\">\n        <label for=\"qualification\" class=\"col-lg-2 text-right\">Qualification:</label>\n        <input type=\"text\" class=\"form-control  col-lg-9\" id=\"qualification\" placeholder=\"Qualification\" name=\"qualification\">\n        <span class=\"col-lg-1\"></span>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-lg-11 text-right\">\n          <button type=\"submit\" class=\"btn btn-default\">Submit</button>\n        </div>\n        <div class=\"col-lg-1\"></div>\n      </div>\n    </form>\n</div>\n"

/***/ }),

/***/ "./src/app/yeluguri-careers/yeluguri-careers.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/yeluguri-careers/yeluguri-careers.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/yeluguri-careers/yeluguri-careers.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/yeluguri-careers/yeluguri-careers.component.ts ***!
  \****************************************************************/
/*! exports provided: YeluguriCareersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YeluguriCareersComponent", function() { return YeluguriCareersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var YeluguriCareersComponent = /** @class */ (function () {
    function YeluguriCareersComponent() {
        this.eventData = { name: '', email: '', phone: '', city: '', more: '', mainEvent: 'Select Event', eventData: [] };
    }
    YeluguriCareersComponent.prototype.ngOnInit = function () {
    };
    YeluguriCareersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-yeluguri-careers',
            template: __webpack_require__(/*! ./yeluguri-careers.component.html */ "./src/app/yeluguri-careers/yeluguri-careers.component.html"),
            styles: [__webpack_require__(/*! ./yeluguri-careers.component.scss */ "./src/app/yeluguri-careers/yeluguri-careers.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], YeluguriCareersComponent);
    return YeluguriCareersComponent;
}());



/***/ }),

/***/ "./src/app/yeluguri-entertainments/yeluguri-entertainments.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/yeluguri-entertainments/yeluguri-entertainments.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style='height:500px;'>\n  <mdb-carousel [isControls]=\"true\" class=\"carousel slide\"  [animation]=\"'slide'\">\n      <mdb-carousel-item *ngFor=\"let img of homePageData.sliders; index as i;\">\n          <img class=\"d-block w-100\" src=\"{{img}}\" alt=\"yeluguri images\">\n      </mdb-carousel-item>\n  </mdb-carousel>\n</div>\n\n<div class=\"container\">\n  <div class=\"row\" style='margin-top:40px;margin-bottom:40px;'>\n    <div class=\"class-sm-12 col-md-6\">\n      <img src=\"{{homePageData.about.img}}\" style=\"height:300px;\"/>\n    </div>\n    <div class=\"class-sm-12 col-md-6\">\n        <p class=\"text-center\" innerHTML=\"{{homePageData.about.details}}\"></p>\n    </div>\n  </div>\n\n  <div class=\"row\" style='margin-top:60px;margin-bottom:40px;'>\n      <div class=\"col\">\n        <h1 class=\"text-center\">TEASERS</h1> \n      </div>\n  </div>\n\n  <div class=\"row\">\n      <div class=\"class-sm-12 col-md-6\" style='margin-bottom:40px;' *ngFor=\"let item of homePageData.teasers\" (click)=\"showVideo(item)\">\n        <img src=\"{{item.img}}\" style=\"width:100%;height:400px;\"/>\n      </div>\n  </div>\n\n  <div mdbModal #videoModal=\"mdbModal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBasicModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\" style='padding:50px;'>\n          <div class=\"modal-header\">\n            <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"videoModal.hide()\">\n              <span aria-hidden=\"true\">×</span>\n            </button>\n          </div>\n        <iframe width=\"420\" height=\"315\" [src]='sanitizer.bypassSecurityTrustResourceUrl(selectedVideo)'></iframe>\n      </div>\n    </div>\n  </div>\n\n  <hr/>\n  \n</div>\n"

/***/ }),

/***/ "./src/app/yeluguri-entertainments/yeluguri-entertainments.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/yeluguri-entertainments/yeluguri-entertainments.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".carousel-inner {\n  height: 500px !important; }\n"

/***/ }),

/***/ "./src/app/yeluguri-entertainments/yeluguri-entertainments.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/yeluguri-entertainments/yeluguri-entertainments.component.ts ***!
  \******************************************************************************/
/*! exports provided: YeluguriEntertainmentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YeluguriEntertainmentsComponent", function() { return YeluguriEntertainmentsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var YeluguriEntertainmentsComponent = /** @class */ (function () {
    function YeluguriEntertainmentsComponent(afs, sanitizer) {
        var _this = this;
        this.afs = afs;
        this.sanitizer = sanitizer;
        this.selectedVideo = '';
        this.homePageData = { copyright: '', sliders: [], facebook: '', instagram: '', youtube: '', logo: '', phone: '', teasers: [], email: '', about: { img: '', details: '' }, bookus: '', joinus: '', rentastudio: '' };
        this.itemsCollection = afs.collection('info');
        this.itemDoc = afs.doc('info/NtRkKdFFCIXYCnvMHO78');
        this.itemsd = this.itemsCollection.valueChanges();
        this.itemsd.subscribe(function (data) {
            _this.homePageData = data[0];
        });
    }
    YeluguriEntertainmentsComponent.prototype.showVideo = function (item) {
        console.log(item.videourl);
        this.selectedVideo = item.videourl.replace("watch?v=", "embed/");
        console.log(this.selectedVideo);
        this.videoModal.show();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('videoModal'),
        __metadata("design:type", Object)
    ], YeluguriEntertainmentsComponent.prototype, "videoModal", void 0);
    YeluguriEntertainmentsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-yeluguri-entertainments',
            template: __webpack_require__(/*! ./yeluguri-entertainments.component.html */ "./src/app/yeluguri-entertainments/yeluguri-entertainments.component.html"),
            styles: [__webpack_require__(/*! ./yeluguri-entertainments.component.scss */ "./src/app/yeluguri-entertainments/yeluguri-entertainments.component.scss")]
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]])
    ], YeluguriEntertainmentsComponent);
    return YeluguriEntertainmentsComponent;
}());



/***/ }),

/***/ "./src/app/yeluguri-rentastudio/yeluguri-rentastudio.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/yeluguri-rentastudio/yeluguri-rentastudio.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  yeluguri-rentastudio works!\n</p>\n"

/***/ }),

/***/ "./src/app/yeluguri-rentastudio/yeluguri-rentastudio.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/yeluguri-rentastudio/yeluguri-rentastudio.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/yeluguri-rentastudio/yeluguri-rentastudio.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/yeluguri-rentastudio/yeluguri-rentastudio.component.ts ***!
  \************************************************************************/
/*! exports provided: YeluguriRentastudioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YeluguriRentastudioComponent", function() { return YeluguriRentastudioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var YeluguriRentastudioComponent = /** @class */ (function () {
    function YeluguriRentastudioComponent() {
    }
    YeluguriRentastudioComponent.prototype.ngOnInit = function () {
    };
    YeluguriRentastudioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-yeluguri-rentastudio',
            template: __webpack_require__(/*! ./yeluguri-rentastudio.component.html */ "./src/app/yeluguri-rentastudio/yeluguri-rentastudio.component.html"),
            styles: [__webpack_require__(/*! ./yeluguri-rentastudio.component.scss */ "./src/app/yeluguri-rentastudio/yeluguri-rentastudio.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], YeluguriRentastudioComponent);
    return YeluguriRentastudioComponent;
}());



/***/ }),

/***/ "./src/app/yeluguri-weddings/yeluguri-weddings.component.html":
/*!********************************************************************!*\
  !*** ./src/app/yeluguri-weddings/yeluguri-weddings.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row text-center\" style=\"margin-top:30px;margin-bottom:8px;\">\n        <h1 style=\"width:100%;border-bottom: 5px solid #ff7043;padding-bottom: 8px;\">WEDDINGS</h1>\n    </div>\n    <div class=\"row\" style=\"margin-bottom:40px;\" class=\"wedding-text\">\n        <blockquote>\n            {{page.wedding}}\n        </blockquote>\n    </div>\n    <div class=\"row\"> \n        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" align=\"center\">\n            <button class=\"btn btn-unique waves-effect waves-light filter-button\" id=\"all\">All</button>\n            <button class=\"btn btn-pink waves-effect waves-light filter-button\" id=\"{{item.name}}\"   *ngFor=\"let item of weddingData.weddings;\">{{item.name}}</button>\n        </div>\n    </div>\n    <!-- All Category-->\n    <div class=\"row category wedding-category\" style='border:1px solid #000;'  *ngFor=\"let item of weddingData.weddings;\">\n        <div class=\"gallery_product col-sm-12 filter {{item.name}}\">\n            <h3>{{item.name}}</h3>\n            <a href=\"{{item.more}}\" target=\"_blank\"></a>\n        </div>\n        <div class=\"gallery_product col-lg-12 col-md-12 col-sm-12 col-xs-12 filter {{item.name}}\">\n            <!--<a href=\"{{img}}\" data-lightbox=\"{{item.name}}\">\n                <img src=\"{{img}}\" class=\"home-img\"/>\n            </a>-->\n            <mdb-image-modal [modalImages]=\"item.images\" type=\"margin\"></mdb-image-modal>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/yeluguri-weddings/yeluguri-weddings.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/yeluguri-weddings/yeluguri-weddings.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/yeluguri-weddings/yeluguri-weddings.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/yeluguri-weddings/yeluguri-weddings.component.ts ***!
  \******************************************************************/
/*! exports provided: YeluguriWeddingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YeluguriWeddingsComponent", function() { return YeluguriWeddingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var YeluguriWeddingsComponent = /** @class */ (function () {
    function YeluguriWeddingsComponent(afs) {
        var _this = this;
        this.afs = afs;
        this.page = { about: '', book: '', album: '', wedding: '' };
        this.weddingData = { weddings: [] };
        this.itemsCollectionw = afs.collection('page');
        this.itemDocw = afs.doc('page/EI9bBobft8w6GCLL3s2L');
        this.itemsdw = this.itemsCollectionw.valueChanges();
        this.itemsdw.subscribe(function (data) {
            _this.page = data[0];
        });
        this.itemsCollection = afs.collection('weddings');
        this.itemDoc = afs.doc('weddings/iyCWnLXI0AvEzj8cyPim');
        this.itemsd = this.itemsCollection.valueChanges();
        this.itemsd.subscribe(function (data) {
            _this.weddingData = data[0];
            for (var i = 0; i < _this.weddingData.weddings.length; i++) {
                var images = _this.weddingData.weddings[i].images;
                var arr = [];
                for (var j = 0; j < images.length; j++) {
                    arr.push({ img: images[j], thumb: images[j], description: j });
                }
                _this.weddingData.weddings[i].images = arr;
            }
            setTimeout(function () {
                jquery__WEBPACK_IMPORTED_MODULE_1__(".filter-button").on("click", function () {
                    var value = jquery__WEBPACK_IMPORTED_MODULE_1__(this).attr('id');
                    if (value == "all") {
                        jquery__WEBPACK_IMPORTED_MODULE_1__('.filter').show('1000');
                    }
                    else {
                        jquery__WEBPACK_IMPORTED_MODULE_1__(".filter").not('.' + value).hide('3000');
                        jquery__WEBPACK_IMPORTED_MODULE_1__('.filter').filter('.' + value).show('3000');
                    }
                    jquery__WEBPACK_IMPORTED_MODULE_1__(".filter-button").removeClass("active");
                    jquery__WEBPACK_IMPORTED_MODULE_1__('#' + value).addClass("active");
                });
                /*if ($(".filter-button").removeClass("active")) {
                  $(this).removeClass("active");
                }*/
            }, 3000);
        });
    }
    YeluguriWeddingsComponent.prototype.ngOnInit = function () {
    };
    YeluguriWeddingsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-yeluguri-weddings',
            template: __webpack_require__(/*! ./yeluguri-weddings.component.html */ "./src/app/yeluguri-weddings/yeluguri-weddings.component.html"),
            styles: [__webpack_require__(/*! ./yeluguri-weddings.component.scss */ "./src/app/yeluguri-weddings/yeluguri-weddings.component.scss")]
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"]])
    ], YeluguriWeddingsComponent);
    return YeluguriWeddingsComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyBgz8jLeK34MniB0h7w-Zm08JY4yWgtsTM",
        authDomain: "yeluguri-35a53.firebaseapp.com",
        databaseURL: "https://yeluguri-35a53.firebaseio.com",
        projectId: "yeluguri-35a53",
        storageBucket: "yeluguri-35a53.appspot.com",
        messagingSenderId: "595073759408"
    }
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
document.addEventListener('DOMContentLoaded', function () {
    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
        .catch(function (err) { return console.log(err); });
});


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/sanjith/Desktop/yeluguri/dynamic/yeluguri/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map