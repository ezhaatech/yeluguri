const functions = require('firebase-functions');
const cors = require("cors");
const express = require("express");
var bodyParser = require('body-parser');
var nodemailer = require('nodemailer');

const app = express()
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

app.use(cors({ origin: true }))
app.post("*", (request, response) => {
    console.log(request.body)

    var html = request.body.html
    var to =  request.body.to

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'yeluguribookings@gmail.com',
            pass: 'yeluguri@124'
        }
    });
    
    var mailOptions = {
        from: 'yeluguribookings@gmail.com',
        to: to,
        subject: 'Yeluguri Entertainments - Thanks for Booking Event',
        html: html
    };
    
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
            response.setHeader('Content-Type', 'application/json');
            response.send(JSON.stringify({ status: 'error' }));
        } else {
            response.setHeader('Content-Type', 'application/json');
            response.send(JSON.stringify({ status: 'success' }));
        }
    });
})

const api = functions.https.onRequest((request, response) => {
    if (!request.path) {
      request.url = `/${request.url}` // prepend '/' to keep query params if any
    }
    return app(request, response)
  })

module.exports = {  api  }