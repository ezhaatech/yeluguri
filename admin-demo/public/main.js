(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/albums/albums.component.html":
/*!**********************************************!*\
  !*** ./src/app/albums/albums.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-sm-12 col-md-3\">\n        <div class=\"md-form\">\n          <input mdbInputDirective type=\"text\" id=\"form1\" class=\"form-control\" [(ngModel)]=\"albumName\">\n          <label for=\"form1\" class=\"\">Enter Album Name</label>\n        </div>\n    </div>\n    <div class=\"col-sm-12 col-md-3\">\n        <div class=\"md-form\">\n            <button type=\"button\" class=\"btn btn-warning btn-md waves-light\" mdbWavesEffect (click)='addAlbum()'>Add Album</button>\n        </div>\n    </div>\n    <div class=\"col-sm-12 c0l-md-6\">\n    </div>\n  </div>\n\n  <div class=\"row\" *ngIf=\"albumData.albums.length > 0\">\n    <div class=\"col\">\n        <div class=\"btn-group\" mdbDropdown>\n            <button mdbDropdownToggle type=\"button\" class=\"btn btn-primary dropdown-toggle waves-light\" mdbWavesEffect>\n                <span *ngIf='selectedName == \"\"'>\n                  Select Album\n                </span>\n                <span *ngIf='selectedName !== \"\"'>\n                    {{selectedName}}\n                </span>\n            </button>\n        \n            <div class=\"dropdown-menu dropdown-primary\">\n                <div  *ngFor=\"let item of albumData.albums; let i = index\">\n\n                  <a>\n                    <span class=\"dropdown-item\" (click)=\"selectAlbum(i,item)\">{{item.name}}</span>\n                      <br/>\n                    <span (click)=\"deleteAlbum(i,item)\" style='margin-left: 20px;'>Delete</span>\n                  </a>\n                  <hr/>\n                </div>\n            </div>\n        </div>\n    </div>\n  </div>\n\n  <div class=\"row\" *ngIf=\"selectedAlbum.length>0\">\n    <div class=\"col\">\n        <table class=\"table\">\n          <tbody>\n            <tr *ngFor=\"let item of selectedAlbum; let i = index\">\n              <td>\n                <img src='{{item}}' style='width:100px;height:100px'/>\n              </td>\n              <td>\n                <button (click)=\"deleteImage(i,item)\">Delete</button>\n              </td>\n            </tr>\n          </tbody>\n        </table>\n    </div>\n  </div>\n\n  <div class=\"row\"  *ngIf='selectedName !== \"\"'>\n    <div class=\"col-sm-12\">\n        <button type=\"button\" class=\"btn btn-primary btn-md waves-light\" mdbWavesEffect (click)=\"addAlbumImage()\">Add Album Image</button>\n    </div>\n  </div>\n\n  <div mdbModal #sliderModal=\"mdbModal\" class=\"modal fade right\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBasicModalLabel\" aria-hidden=\"true\">\n      <div class=\"modal-dialog modal-side modal-top-right\" role=\"document\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\" style='height:500px;overflow:scroll;'>\n              <div class=\"col-sm-12\" *ngFor=\"let img of imgData.images; index as i;\" style='margin:0px;margin-bottom:20px;border:1px solid #ccc;padding:0px;margin-right:10px;'>\n                  <img src=\"{{img}}\" style='height:200px;object-fit: cover' *ngIf=\"img!=''\" (click)=\"selectSlider(i)\"/>\n              </div>\n          </div>\n          <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-secondary btn-sm  waves-light\" aria-label=\"Close\" (click)=\"sliderModal.hide()\" mdbWavesEffect>Close</button>\n          </div>\n        </div>\n      </div>\n    </div>\n\n  <div mdbModal #loadingModal=\"mdbModal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBasicModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-body\">\n          <div class=\"loader\">Loading...</div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/albums/albums.component.scss":
/*!**********************************************!*\
  !*** ./src/app/albums/albums.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/albums/albums.component.ts":
/*!********************************************!*\
  !*** ./src/app/albums/albums.component.ts ***!
  \********************************************/
/*! exports provided: AlbumsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlbumsComponent", function() { return AlbumsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AlbumsComponent = /** @class */ (function () {
    function AlbumsComponent(afs) {
        var _this = this;
        this.afs = afs;
        this.albumName = '';
        this.selectedAlbum = [];
        this.selectedName = '';
        this.index = 0;
        this.imgData = { images: [] };
        this.albumData = { albums: [] };
        this.itemsCollection = afs.collection('albums');
        this.itemDoc = afs.doc('albums/IBsOvBKsVE9SA8lnyq4i');
        this.itemsd = this.itemsCollection.valueChanges();
        this.itemsd.subscribe(function (data) {
            _this.albumData = data[0];
        });
    }
    AlbumsComponent.prototype.ngOnInit = function () { };
    AlbumsComponent.prototype.addAlbumImage = function () {
        var _this = this;
        this.imgCollection = this.afs.collection('images');
        this.imgDoc = this.afs.doc('images/PxortbUVj3h5sxHbf5a6');
        this.imgd = this.imgCollection.valueChanges();
        this.imgd.subscribe(function (data) {
            _this.imgData = data[0];
            _this.sliderModal.show();
        });
    };
    AlbumsComponent.prototype.selectSlider = function (index) {
        this.sliderModal.hide();
        this.selectedAlbum.push(this.imgData.images[index]);
        this.updateAlbums();
    };
    AlbumsComponent.prototype.addAlbum = function () {
        if (this.albumName != '') {
            this.albumData.albums.push({ name: this.albumName, albumImages: [] });
            this.albumName = '';
            this.updateAlbums();
        }
    };
    AlbumsComponent.prototype.selectAlbum = function (i, item) {
        console.log(item);
        this.index = i;
        this.selectedName = item.name;
        this.selectedAlbum = item.albumImages;
    };
    AlbumsComponent.prototype.deleteImage = function (i, item) {
        this.selectedAlbum.splice(i, 1);
        this.updateAlbums();
    };
    AlbumsComponent.prototype.deleteAlbum = function (k, item) {
        this.selectedName = '';
        this.selectedAlbum = [];
        this.albumData.albums.splice(k, 1);
        this.updateAlbums();
    };
    AlbumsComponent.prototype.updateAlbums = function () {
        console.log(this.albumData);
        this.loadingModal.show();
        this.itemDoc.update(this.albumData);
        var self = this;
        setTimeout(function () {
            self.loadingModal.hide();
        }, 3000);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sliderModal'),
        __metadata("design:type", Object)
    ], AlbumsComponent.prototype, "sliderModal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('loadingModal'),
        __metadata("design:type", Object)
    ], AlbumsComponent.prototype, "loadingModal", void 0);
    AlbumsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-albums',
            template: __webpack_require__(/*! ./albums.component.html */ "./src/app/albums/albums.component.html"),
            styles: [__webpack_require__(/*! ./albums.component.scss */ "./src/app/albums/albums.component.scss")]
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]])
    ], AlbumsComponent);
    return AlbumsComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _homepageinfo_homepageinfo_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./homepageinfo/homepageinfo.component */ "./src/app/homepageinfo/homepageinfo.component.ts");
/* harmony import */ var _events_events_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./events/events.component */ "./src/app/events/events.component.ts");
/* harmony import */ var _albums_albums_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./albums/albums.component */ "./src/app/albums/albums.component.ts");
/* harmony import */ var _weddings_weddings_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./weddings/weddings.component */ "./src/app/weddings/weddings.component.ts");
/* harmony import */ var _pages_pages_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/pages.component */ "./src/app/pages/pages.component.ts");
/* harmony import */ var _bookings_bookings_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./bookings/bookings.component */ "./src/app/bookings/bookings.component.ts");
/* harmony import */ var _jobs_jobs_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./jobs/jobs.component */ "./src/app/jobs/jobs.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _images_images_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./images/images.component */ "./src/app/images/images.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var routes = [
    { path: '', redirectTo: '/info', pathMatch: 'full' },
    {
        path: 'info',
        component: _homepageinfo_homepageinfo_component__WEBPACK_IMPORTED_MODULE_2__["HomepageinfoComponent"]
    },
    {
        path: 'events',
        component: _events_events_component__WEBPACK_IMPORTED_MODULE_3__["EventsComponent"]
    },
    {
        path: 'albums',
        component: _albums_albums_component__WEBPACK_IMPORTED_MODULE_4__["AlbumsComponent"]
    },
    {
        path: 'weddings',
        component: _weddings_weddings_component__WEBPACK_IMPORTED_MODULE_5__["WeddingsComponent"]
    },
    {
        path: 'pages',
        component: _pages_pages_component__WEBPACK_IMPORTED_MODULE_6__["PagesComponent"]
    },
    {
        path: 'bookings',
        component: _bookings_bookings_component__WEBPACK_IMPORTED_MODULE_7__["BookingsComponent"]
    },
    {
        path: 'jobs',
        component: _jobs_jobs_component__WEBPACK_IMPORTED_MODULE_8__["JobsComponent"]
    },
    {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"]
    },
    {
        path: 'images',
        component: _images_images_component__WEBPACK_IMPORTED_MODULE_10__["ImagesComponent"]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid elegant-color-dark\">\n\n    <!-- Navbar Start here -->\n    <div class=\"container\">\n        <mdb-navbar SideClass=\"navbar navbar-expand-lg navbar-dark elegant-color-dark\">\n            <mdb-navbar-brand><a class=\"navbar-brand\" routerLink=\"/info\">Yeluguri</a></mdb-navbar-brand>\n            <links>\n                <ul class=\"navbar-nav mr-auto\">\n                    <li class=\"nav-item\" [routerLinkActive]=\"['activeC']\">\n                        <a class=\"nav-link waves-light\" mdbWavesEffect routerLink=\"/info\">\n                            <i class=\"fa fa-home\"></i>\n                        </a>\n                    </li>\n                    <li class=\"nav-item\" [routerLinkActive]=\"['activeC']\">\n                        <a class=\"nav-link waves-light\" mdbWavesEffect routerLink=\"/weddings\">WEDDINGS</a>\n                    </li>\n                    <li class=\"nav-item\" [routerLinkActive]=\"['activeC']\">\n                        <a class=\"nav-link waves-light\" mdbWavesEffect routerLink=\"/albums\">ALBUMS</a>\n                    </li>\n                    <li class=\"nav-item\" [routerLinkActive]=\"['activeC']\">\n                        <a class=\"nav-link waves-light\" mdbWavesEffect routerLink=\"/events\">EVENTS</a>\n                    </li>\n                    <!--<li class=\"nav-item\" [routerLinkActive]=\"['activeC']\">\n                        <a class=\"nav-link waves-light\" mdbWavesEffect routerLink=\"/jobs\">JOBS</a>\n                    </li>-->\n                    <li class=\"nav-item\" [routerLinkActive]=\"['activeC']\">\n                        <a class=\"nav-link waves-light\" mdbWavesEffect routerLink=\"/pages\">PAGES</a>\n                    </li>\n                    <li class=\"nav-item\" [routerLinkActive]=\"['activeC']\">\n                        <a class=\"nav-link waves-light\" mdbWavesEffect routerLink=\"/images\">IMAGES</a>\n                    </li>\n                </ul>\n            </links>\n        </mdb-navbar>\n    </div>\n    <!-- Navbar End here -->\n</div>\n\n<div class=\"container\">\n    <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'yeluguri';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular-bootstrap-md */ "./node_modules/angular-bootstrap-md/esm5/angular-bootstrap-md.es5.js");
/* harmony import */ var ng_pick_datetime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng-pick-datetime */ "./node_modules/ng-pick-datetime/picker.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _homepageinfo_homepageinfo_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./homepageinfo/homepageinfo.component */ "./src/app/homepageinfo/homepageinfo.component.ts");
/* harmony import */ var _events_events_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./events/events.component */ "./src/app/events/events.component.ts");
/* harmony import */ var _albums_albums_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./albums/albums.component */ "./src/app/albums/albums.component.ts");
/* harmony import */ var _weddings_weddings_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./weddings/weddings.component */ "./src/app/weddings/weddings.component.ts");
/* harmony import */ var _pages_pages_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pages/pages.component */ "./src/app/pages/pages.component.ts");
/* harmony import */ var _bookings_bookings_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./bookings/bookings.component */ "./src/app/bookings/bookings.component.ts");
/* harmony import */ var _jobs_jobs_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./jobs/jobs.component */ "./src/app/jobs/jobs.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var angularfire2__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! angularfire2 */ "./node_modules/angularfire2/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! angularfire2/storage */ "./node_modules/angularfire2/storage/index.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
/* harmony import */ var _images_images_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./images/images.component */ "./src/app/images/images.component.ts");
/* harmony import */ var ngx_editor__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-editor */ "./node_modules/ngx-editor/esm5/ngx-editor.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _homepageinfo_homepageinfo_component__WEBPACK_IMPORTED_MODULE_8__["HomepageinfoComponent"],
                _events_events_component__WEBPACK_IMPORTED_MODULE_9__["EventsComponent"],
                _albums_albums_component__WEBPACK_IMPORTED_MODULE_10__["AlbumsComponent"],
                _weddings_weddings_component__WEBPACK_IMPORTED_MODULE_11__["WeddingsComponent"],
                _pages_pages_component__WEBPACK_IMPORTED_MODULE_12__["PagesComponent"],
                _bookings_bookings_component__WEBPACK_IMPORTED_MODULE_13__["BookingsComponent"],
                _jobs_jobs_component__WEBPACK_IMPORTED_MODULE_14__["JobsComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_15__["LoginComponent"],
                _images_images_component__WEBPACK_IMPORTED_MODULE_21__["ImagesComponent"]
            ],
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                ngx_editor__WEBPACK_IMPORTED_MODULE_22__["NgxEditorModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_23__["HttpClientModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_4__["MDBBootstrapModule"].forRoot(),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                ng_pick_datetime__WEBPACK_IMPORTED_MODULE_5__["OwlDateTimeModule"],
                ng_pick_datetime__WEBPACK_IMPORTED_MODULE_5__["OwlNativeDateTimeModule"],
                angularfire2__WEBPACK_IMPORTED_MODULE_16__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_17__["environment"].firebase),
                angularfire2_firestore__WEBPACK_IMPORTED_MODULE_18__["AngularFirestoreModule"],
                angularfire2_auth__WEBPACK_IMPORTED_MODULE_20__["AngularFireAuthModule"],
                angularfire2_storage__WEBPACK_IMPORTED_MODULE_19__["AngularFireStorageModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"].withServerTransition({ appId: 'serverApp' }),
                _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"]
            ],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["NO_ERRORS_SCHEMA"]],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/bookings/bookings.component.html":
/*!**************************************************!*\
  !*** ./src/app/bookings/bookings.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  bookings works!\n</p>\n"

/***/ }),

/***/ "./src/app/bookings/bookings.component.scss":
/*!**************************************************!*\
  !*** ./src/app/bookings/bookings.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/bookings/bookings.component.ts":
/*!************************************************!*\
  !*** ./src/app/bookings/bookings.component.ts ***!
  \************************************************/
/*! exports provided: BookingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookingsComponent", function() { return BookingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BookingsComponent = /** @class */ (function () {
    function BookingsComponent() {
    }
    BookingsComponent.prototype.ngOnInit = function () {
    };
    BookingsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bookings',
            template: __webpack_require__(/*! ./bookings.component.html */ "./src/app/bookings/bookings.component.html"),
            styles: [__webpack_require__(/*! ./bookings.component.scss */ "./src/app/bookings/bookings.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BookingsComponent);
    return BookingsComponent;
}());



/***/ }),

/***/ "./src/app/events/events.component.html":
/*!**********************************************!*\
  !*** ./src/app/events/events.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-sm-12 col-md-3\">\n          <div class=\"md-form\">\n            <input mdbInputDirective type=\"text\" id=\"form1\" class=\"form-control\" [(ngModel)]=\"mainEventName\">\n            <label for=\"form1\" class=\"\">Enter Main Event Name</label>\n          </div>\n      </div>\n      <div class=\"col-sm-12 col-md-3\">\n          <div class=\"md-form\">\n              <button type=\"button\" class=\"btn btn-warning btn-md waves-light\" mdbWavesEffect (click)='addEventName()'>Add Event</button>\n          </div>\n      </div>\n      <div class=\"col-sm-12 c0l-md-6\">\n      </div>\n    </div>\n  \n    <div class=\"row\" *ngIf=\"eventData.events.length > 0\">\n      <div class=\"col\">\n          <div class=\"btn-group\" mdbDropdown>\n              <button mdbDropdownToggle type=\"button\" class=\"btn btn-primary dropdown-toggle waves-light\" mdbWavesEffect>\n                  <span *ngIf='selectedName == \"\"'>\n                    Select Event\n                  </span>\n                  <span *ngIf='selectedName !== \"\"'>\n                      {{selectedName}}\n                  </span>\n              </button>\n          \n              <div class=\"dropdown-menu dropdown-primary\">\n                  <div  *ngFor=\"let item of eventData.events; let i = index\">\n                    <a >\n                      <span class=\"dropdown-item\" (click)=\"selectEvent(item)\">{{item.mainEventName}}</span>\n                        <br/>\n                      <span (click)=\"deleteEvent(i)\">Delete</span>\n                    </a>\n                    <hr/>\n                  </div>\n              </div>\n          </div>\n      </div>\n    </div>\n  \n    <div class=\"row\" *ngIf=\"selectedEvent.length>0\">\n      <div class=\"col\">\n          <table class=\"table\">\n            <tbody>\n              <tr *ngFor=\"let item of selectedEvent; let i = index\">\n                <td>\n                  {{item}}\n                </td>\n                <td>\n                  <button (click)=\"deleteSubEvent(i)\">Delete</button>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n      </div>\n    </div>\n  \n    <div class=\"row\" *ngIf=\"selectedName!=''\">\n      <div class=\"col-sm-12 col-md-3\">\n          <div class=\"md-form\">\n            <input mdbInputDirective type=\"text\" id=\"form2\" class=\"form-control\" [(ngModel)]=\"subEventName\">\n            <label for=\"form2\" class=\"\">Enter Sub Main Event Name</label>\n          </div>\n          <div class=\"md-form\">\n              <button type=\"button\" class=\"btn btn-warning btn-md waves-light\" mdbWavesEffect (click)='addSubEventName()'>Add Sub Event</button>\n          </div>\n      </div>\n    </div>\n\n    <div mdbModal #loadingModal=\"mdbModal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBasicModalLabel\" aria-hidden=\"true\">\n      <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\">\n            <div class=\"loader\">Loading...</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  \n  </div>"

/***/ }),

/***/ "./src/app/events/events.component.scss":
/*!**********************************************!*\
  !*** ./src/app/events/events.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/events/events.component.ts":
/*!********************************************!*\
  !*** ./src/app/events/events.component.ts ***!
  \********************************************/
/*! exports provided: EventsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventsComponent", function() { return EventsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EventsComponent = /** @class */ (function () {
    function EventsComponent(afs) {
        var _this = this;
        this.afs = afs;
        this.eventData = { events: [] };
        this.mainEventName = '';
        this.subEventName = '';
        this.selectedEvent = [];
        this.selectedName = '';
        this.itemsCollection = afs.collection('events');
        this.itemDoc = afs.doc('events/AKyy6XfUCjsmyJphGUeg');
        this.itemsd = this.itemsCollection.valueChanges();
        this.itemsd.subscribe(function (data) {
            _this.eventData = data[0];
        });
    }
    EventsComponent.prototype.ngOnInit = function () { };
    EventsComponent.prototype.addEventName = function () {
        if (this.mainEventName != '') {
            this.eventData.events.push({ mainEventName: this.mainEventName, subEvents: [] });
            this.mainEventName = '';
            this.updateEvent();
        }
    };
    EventsComponent.prototype.selectEvent = function (item) {
        this.selectedName = item.mainEventName;
        this.selectedEvent = item.subEvents;
    };
    EventsComponent.prototype.addSubEventName = function () {
        if (this.subEventName != '') {
            this.selectedEvent.push(this.subEventName);
            this.subEventName = '';
            this.updateEvent();
        }
    };
    EventsComponent.prototype.deleteSubEvent = function (k, item) {
        this.selectedEvent.splice(k, 1);
        this.updateEvent();
    };
    EventsComponent.prototype.deleteEvent = function (i) {
        this.selectedName = '';
        this.selectedEvent = [];
        this.eventData.events.splice(i, 1);
        this.updateEvent();
    };
    EventsComponent.prototype.updateEvent = function () {
        console.log(this.eventData);
        this.loadingModal.show();
        this.itemDoc.update(this.eventData);
        var self = this;
        setTimeout(function () {
            self.loadingModal.hide();
        }, 2000);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('loadingModal'),
        __metadata("design:type", Object)
    ], EventsComponent.prototype, "loadingModal", void 0);
    EventsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-events',
            template: __webpack_require__(/*! ./events.component.html */ "./src/app/events/events.component.html"),
            styles: [__webpack_require__(/*! ./events.component.scss */ "./src/app/events/events.component.scss")]
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]])
    ], EventsComponent);
    return EventsComponent;
}());



/***/ }),

/***/ "./src/app/homepageinfo/homepageinfo.component.html":
/*!**********************************************************!*\
  !*** ./src/app/homepageinfo/homepageinfo.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-sm-12\">\n        <br/> \n        <button type=\"button\" class=\"btn btn-md btn-outline-primary\" (click)=\"showInfo = true;showSliders=false;showTeasers=false;showAbout=false\" mdbWavesEffect>Info</button>\n        <button type=\"button\" class=\"btn btn-md btn-outline-default\" (click)=\"showInfo = false;showSliders=true;showTeasers=false;showAbout=false\" mdbWavesEffect>Sliders</button>\n        <button type=\"button\" class=\"btn btn-md btn-outline-secondary\" (click)=\"showInfo = false;showSliders=false;showTeasers=true;showAbout=false\" mdbWavesEffect>Teasers</button>\n        <button type=\"button\" class=\"btn btn-md btn-outline-secondary\" (click)=\"showInfo = false;showSliders=false;showTeasers=false;showAbout=true\" mdbWavesEffect>About</button>\n        \n        <br/>\n        <div class=\"loader\" *ngIf=\"showLoading\">Loading...</div>\n\n        <div *ngIf=\"showSliders\">\n          <br/><br/>\n          <button type=\"button\" class=\"btn btn-primary btn-md waves-light\" mdbWavesEffect (click)=\"addSlider()\">Add Slider Images</button>\n          <br/>\n          <div class=\"row\">\n            <div class=\"col-sm-12 col-md-2\" *ngFor=\"let item of homePageData.sliders; let i = index\" style='border:1px solid #ccc;margin-bottom:20px;margin-right:5px;'>\n                <img src='{{item}}' style='width:100%;height:200px;'/>\n                <br/>\n                <button type=\"button\" (click)=\"deleteSlider(i)\" class=\"btn btn-danger  btn-sm btn-rounded waves-light\" mdbWavesEffect>Delete</button>\n            </div>\n          </div>\n        </div>\n        <div *ngIf=\"showTeasers\">\n          <br/><br/>\n            <div style='border:1px solid #eee;'>\n            <div class=\"md-form\">\n                <input mdbInputDirective type=\"text\" id=\"form1\" class=\"form-control\" [(ngModel)]=\"teaserVideoUrl\">\n                <label for=\"form1\" class=\"\">Youtube Video Url</label>\n              </div>\n              <div *ngIf=\"teaserImg != ''\">\n                <img src=\"{{teaserImg}}\" style='width:100px;height:100px;'/>\n              </div>\n              <br/>              \n              <button type=\"button\" class=\"btn btn-info btn-sm waves-light\" mdbWavesEffect (click)=\"addTeaserImage()\">Add Teaser Image</button>\n            </div>   <br/> \n          <button type=\"button\" class=\"btn btn-primary btn-md waves-light\" mdbWavesEffect (click)=\"addTeaser()\">Add Teaser</button>\n          <br/> <br/> \n          <table class=\"table\">\n            <tbody>\n              <tr *ngFor=\"let item of homePageData.teasers; let i = index\">\n                <td>\n                    <img src='{{item.img}}' style='width:100px;height:100px;'/>\n                </td>\n                <td>\n                  {{item.videourl}}\n                </td>\n                <td>\n                  <button type=\"button\" (click)=\"deleteTeaser(i)\" class=\"btn btn-danger  btn-sm btn-rounded waves-light\" mdbWavesEffect>Delete</button>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n        <div *ngIf=\"showAbout\">\n            <br/>\n            <div *ngIf=\"homePageData.about.img !=''\">\n              <img src='{{homePageData.about.img}}' style='width:100px;height:100px;'/>\n              <button type=\"button\" (click)=\"aboutImgDelete()\" class=\"btn btn-danger  btn-sm btn-rounded waves-light\" mdbWavesEffect>Delete</button>\n            </div><br/><br/>\n            <div *ngIf=\"homePageData.about.img ==''\">\n                <button type=\"button\" class=\"btn btn-primary btn-md waves-light\" mdbWavesEffect (click)=\"addAboutImage()\">Add About Image</button>\n            </div><br/>\n            <div >\n                <textarea style='width:100%;height:400px;' [placeholder]=\"'Enter text here...'\"  [(ngModel)]=\"homePageData.about.details\"></textarea>\n            </div>\n            <br/>\n        </div>\n        <div *ngIf=\"showInfo\">\n          <div class=\"md-form\" *ngIf=\"homePageData.logo==''\">\n              <button type=\"button\" class=\"btn btn-default btn-md waves-light\" mdbWavesEffect (click)=\"addLogo()\">Add Logo</button>\n          </div>\n          <div class=\"md-form\" *ngIf=\"homePageData.logo!=''\">\n            <img src='{{homePageData.logo}}' style='height:100px;width:100px;'/><br/>\n            <button type=\"button\" (click)=\"deleteLogo()\" class=\"btn btn-danger  btn-sm btn-rounded waves-light\" mdbWavesEffect>delete logo</button>\n          </div>\n          <br/>\n          <div class=\"md-form\">\n            <input type=\"text\" id=\"materialFormCardNameEx\"  class=\"form-control\" mdbInputDirective name='username' [(ngModel)]=\"homePageData.phone\">\n            <label for=\"materialFormCardNameEx\" class=\"font-weight-light\">phone</label>\n          </div>\n          <br/>\n          <div class=\"md-form\">\n            <input type=\"text\" id=\"materialFormCardPasswordEx\"  class=\"form-control\" mdbInputDirective name='password' [(ngModel)]=\"homePageData.email\">\n            <label for=\"materialFormCardPasswordEx\" class=\"font-weight-light\">email</label>\n          </div><br/>\n          <div class=\"md-form\">\n            <input type=\"text\" id=\"materialFormCardNameEx\"  class=\"form-control\" mdbInputDirective name='username' [(ngModel)]=\"homePageData.facebook\">\n            <label for=\"materialFormCardNameEx\" class=\"font-weight-light\">facebook</label>\n          </div><br/>\n          <div class=\"md-form\">\n            <input type=\"text\" id=\"materialFormCardNameEx\"  class=\"form-control\" mdbInputDirective name='username' [(ngModel)]=\"homePageData.instagram\">\n            <label for=\"materialFormCardNameEx\" class=\"font-weight-light\">instagram</label>\n          </div>  <br/>\n          <div class=\"md-form\">\n            <input type=\"text\" id=\"materialFormCardNameEx\"  class=\"form-control\" mdbInputDirective name='username' [(ngModel)]=\"homePageData.youtube\">\n            <label for=\"materialFormCardNameEx\" class=\"font-weight-light\">youtube</label>\n          </div><br/>\n          <div class=\"md-form\">\n            <textarea type=\"text\" id=\"form7\" class=\"md-textarea form-control\" mdbInputDirective [(ngModel)]=\"homePageData.bookus\"></textarea>\n            <label for=\"materialFormCardNameEx\" class=\"font-weight-light\">bookus</label>\n          </div><br/>\n          <div class=\"md-form\">\n            <textarea type=\"text\" id=\"form7\" class=\"md-textarea form-control\" mdbInputDirective [(ngModel)]=\"homePageData.joinus\"></textarea>\n            <label for=\"materialFormCardNameEx\" class=\"font-weight-light\">joinus</label>\n          </div><br/>\n          <div class=\"md-form\">\n            <textarea type=\"text\" id=\"form7\" class=\"md-textarea form-control\" mdbInputDirective  [(ngModel)]=\"homePageData.rentastudio\"></textarea>\n            <label for=\"materialFormCardNameEx\" class=\"font-weight-light\">rentastudio</label>\n          </div><br/>\n          <br/>\n          <div class=\"md-form\">\n            <input type=\"text\" id=\"materialFormCardNameEx\"  class=\"form-control\" mdbInputDirective name='username' [(ngModel)]=\"homePageData.copyright\">\n            <label for=\"materialFormCardNameEx\" class=\"font-weight-light\">copyright</label>\n          </div>\n         </div>\n      </div>\n  \n      <div class=\"col-sm-12 col-md-4\">\n      </div>\n  \n    </div>\n\n    <div class=\"text-center py-4 mt-3\">\n      <button class=\"btn btn-cyan waves-light\" type=\"submit\" mdbWavesEffect (click)='updateData()'>UPDATE</button>\n    </div>\n\n    <div mdbModal #sliderModal=\"mdbModal\" class=\"modal fade right\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBasicModalLabel\" aria-hidden=\"true\">\n      <div class=\"modal-dialog modal-side modal-top-right\" role=\"document\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\" style='height:500px;overflow:scroll;'>\n              <div class=\"col-sm-12\" *ngFor=\"let img of imgData.images; index as i;\" style='margin:0px;margin-bottom:20px;border:1px solid #ccc;padding:0px;margin-right:10px;'>\n                  <img src=\"{{img}}\" style='height:200px;object-fit: cover' *ngIf=\"img!=''\" (click)=\"selectSlider(i)\"/>\n              </div>\n          </div>\n          <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-secondary btn-sm  waves-light\" aria-label=\"Close\" (click)=\"sliderModal.hide()\" mdbWavesEffect>Close</button>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div mdbModal #loadingModal=\"mdbModal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBasicModalLabel\" aria-hidden=\"true\">\n      <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\">\n            <div class=\"loader\">Loading...</div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n  </div>"

/***/ }),

/***/ "./src/app/homepageinfo/homepageinfo.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/homepageinfo/homepageinfo.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/homepageinfo/homepageinfo.component.ts":
/*!********************************************************!*\
  !*** ./src/app/homepageinfo/homepageinfo.component.ts ***!
  \********************************************************/
/*! exports provided: HomepageinfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageinfoComponent", function() { return HomepageinfoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomepageinfoComponent = /** @class */ (function () {
    function HomepageinfoComponent(afs) {
        var _this = this;
        this.afs = afs;
        this.type = 'slider';
        this.showAbout = false;
        this.showTeasers = false;
        this.showInfo = true;
        this.showSliders = false;
        this.teaserImg = '';
        this.teaserVideoUrl = '';
        this.showLoading = true;
        this.imgData = { images: [] };
        this.homePageData = { copyright: '© 2018 Copyright yeluguri entertainments', sliders: [], facebook: '', instagram: '', youtube: '', logo: '', phone: '', teasers: [], email: '', about: { img: '', aboutdetails: '' }, bookus: '', joinus: '', rentastudio: '' };
        this.itemsCollection = afs.collection('info');
        this.itemDoc = afs.doc('info/NtRkKdFFCIXYCnvMHO78');
        this.itemsd = this.itemsCollection.valueChanges();
        this.itemsd.subscribe(function (data) {
            _this.homePageData = data[0];
            _this.showLoading = false;
        });
    }
    HomepageinfoComponent.prototype.ngOnInit = function () { };
    HomepageinfoComponent.prototype.addSlider = function () {
        this.type = 'slider';
        this.openImgModal();
    };
    HomepageinfoComponent.prototype.openImgModal = function () {
        var _this = this;
        this.imgCollection = this.afs.collection('images');
        this.imgDoc = this.afs.doc('images/PxortbUVj3h5sxHbf5a6');
        this.imgd = this.imgCollection.valueChanges();
        this.imgd.subscribe(function (data) {
            _this.imgData = data[0];
            _this.sliderModal.show();
        });
    };
    HomepageinfoComponent.prototype.selectSlider = function (index) {
        this.sliderModal.hide();
        if (this.type === 'slider') {
            this.homePageData.sliders.push(this.imgData.images[index]);
        }
        else if (this.type === 'about') {
            this.homePageData.about.img = this.imgData.images[index];
        }
        else if (this.type === 'teaser') {
            this.teaserImg = this.imgData.images[index];
        }
        else if (this.type === 'logo') {
            this.homePageData.logo = this.imgData.images[index];
        }
    };
    HomepageinfoComponent.prototype.addTeaser = function () {
        this.homePageData.teasers.push({ img: this.teaserImg, videourl: this.teaserVideoUrl });
        this.teaserImg = '';
        this.teaserVideoUrl = '';
    };
    HomepageinfoComponent.prototype.addTeaserImage = function () {
        this.type = 'teaser';
        this.openImgModal();
    };
    HomepageinfoComponent.prototype.addLogo = function () {
        this.type = 'logo';
        this.openImgModal();
    };
    HomepageinfoComponent.prototype.deleteLogo = function () {
        this.homePageData.logo = '';
    };
    HomepageinfoComponent.prototype.addAboutImage = function () {
        this.type = 'about';
        this.openImgModal();
    };
    HomepageinfoComponent.prototype.aboutImgDelete = function () {
        this.homePageData.about.img = '';
    };
    HomepageinfoComponent.prototype.deleteTeaser = function (i) {
        this.homePageData.teasers.splice(i, 1);
    };
    HomepageinfoComponent.prototype.deleteSlider = function (i, item) {
        this.homePageData.sliders.splice(i, 1);
    };
    HomepageinfoComponent.prototype.updateData = function () {
        console.log(this.homePageData);
        this.loadingModal.show();
        this.itemDoc.update(this.homePageData);
        var self = this;
        setTimeout(function () {
            self.loadingModal.hide();
        }, 3000);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sliderModal'),
        __metadata("design:type", Object)
    ], HomepageinfoComponent.prototype, "sliderModal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('loadingModal'),
        __metadata("design:type", Object)
    ], HomepageinfoComponent.prototype, "loadingModal", void 0);
    HomepageinfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-homepageinfo',
            template: __webpack_require__(/*! ./homepageinfo.component.html */ "./src/app/homepageinfo/homepageinfo.component.html"),
            styles: [__webpack_require__(/*! ./homepageinfo.component.scss */ "./src/app/homepageinfo/homepageinfo.component.scss")]
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]])
    ], HomepageinfoComponent);
    return HomepageinfoComponent;
}());



/***/ }),

/***/ "./src/app/images/images.component.html":
/*!**********************************************!*\
  !*** ./src/app/images/images.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br/>\n<div class=\"row\">\n  <div class=\"col-sm-12\">\n      <div class=\"loader\" *ngIf=\"showLoading\">Loading...</div>\n  </div>\n  <br/>\n  <div class=\"col-sm-12\" style=\"border:1px solid #ccc;padding:10px;margin-bottom: 20px;\">\n      <input style=\"display: none\"  type=\"file\" name=\"pic\" accept=\"image/*\" (change)=\"onImageUpload($event)\" #albumImage>\n      <button (click)=\"albumImage.click()\">Upload Image</button>\n  </div>\n  <br/><br/>\n  <div class=\"col-sm-12 col-md-3\" *ngFor=\"let img of imgData.images; index as i;\" style='margin:0px;margin-bottom:20px;border:1px solid #ccc;padding:0px;margin-right:10px;'>\n      <img src=\"{{img}}\" style='width:100%;height:200px;' *ngIf=\"img!=''\"/>\n      <button mdbBtn type=\"button\" color=\"primary btn-danger btn-sm\" class=\"waves-light\" mdbWavesEffect (click)='deleteImg(i)'>delete</button>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/images/images.component.scss":
/*!**********************************************!*\
  !*** ./src/app/images/images.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/images/images.component.ts":
/*!********************************************!*\
  !*** ./src/app/images/images.component.ts ***!
  \********************************************/
/*! exports provided: ImagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImagesComponent", function() { return ImagesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/storage */ "./node_modules/angularfire2/storage/index.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ImagesComponent = /** @class */ (function () {
    function ImagesComponent(afStorage, afs) {
        var _this = this;
        this.afStorage = afStorage;
        this.afs = afs;
        this.imgData = { images: [] };
        this.showLoading = true;
        this.itemsCollection = afs.collection('images');
        this.itemDoc = afs.doc('images/PxortbUVj3h5sxHbf5a6');
        this.itemsd = this.itemsCollection.valueChanges();
        this.itemsd.subscribe(function (data) {
            _this.imgData = data[0];
            _this.showLoading = false;
        });
    }
    ImagesComponent.prototype.ngOnInit = function () {
    };
    ImagesComponent.prototype.onImageUpload = function (event) {
        this.showLoading = true;
        var self = this;
        var randomId = 'yeluguri-images-' + Math.random().toString(36).substring(2) + '.jpg';
        var ref = this.afStorage.ref(randomId);
        var task = ref.put(event.target.files[0]).then(function (result) {
            ref.getDownloadURL().subscribe(function (url) {
                self.imgData.images.push(url);
                self.itemDoc.update(self.imgData);
                self.showLoading = false;
            });
        });
    };
    ImagesComponent.prototype.deleteImg = function (index) {
        this.showLoading = true;
        var self = this;
        this.afStorage.storage.refFromURL(self.imgData.images[index]).delete();
        self.imgData.images.splice(index, 1);
        self.itemDoc.update(self.imgData);
    };
    ImagesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-images',
            template: __webpack_require__(/*! ./images.component.html */ "./src/app/images/images.component.html"),
            styles: [__webpack_require__(/*! ./images.component.scss */ "./src/app/images/images.component.scss")]
        }),
        __metadata("design:paramtypes", [angularfire2_storage__WEBPACK_IMPORTED_MODULE_1__["AngularFireStorage"], angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"]])
    ], ImagesComponent);
    return ImagesComponent;
}());



/***/ }),

/***/ "./src/app/jobs/jobs.component.html":
/*!******************************************!*\
  !*** ./src/app/jobs/jobs.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  jobs works!\n</p>\n"

/***/ }),

/***/ "./src/app/jobs/jobs.component.scss":
/*!******************************************!*\
  !*** ./src/app/jobs/jobs.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/jobs/jobs.component.ts":
/*!****************************************!*\
  !*** ./src/app/jobs/jobs.component.ts ***!
  \****************************************/
/*! exports provided: JobsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobsComponent", function() { return JobsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var JobsComponent = /** @class */ (function () {
    function JobsComponent() {
    }
    JobsComponent.prototype.ngOnInit = function () {
    };
    JobsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-jobs',
            template: __webpack_require__(/*! ./jobs.component.html */ "./src/app/jobs/jobs.component.html"),
            styles: [__webpack_require__(/*! ./jobs.component.scss */ "./src/app/jobs/jobs.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], JobsComponent);
    return JobsComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n\n    <div class=\"col\">\n    </div>\n\n    <div class=\"col\">\n      <br/><br/><br/><br/>\n      <div class=\"card\">\n        <div class=\"card-body\">\n            <p class=\"h4 text-center py-4\">LOGIN</p>\n            <div class=\"md-form\">\n              <i class=\"fa fa-user prefix grey-text\"></i>\n              <input type=\"text\" id=\"materialFormCardNameEx\"  class=\"form-control\" mdbInputDirective name='username' [(ngModel)]=\"username\">\n              <label for=\"materialFormCardNameEx\" class=\"font-weight-light\">name</label>\n            </div>\n            <div class=\"md-form\">\n              <i class=\"fa fa-lock prefix grey-text\"></i>\n              <input type=\"password\" id=\"materialFormCardPasswordEx\"  class=\"form-control\" mdbInputDirective name='password' [(ngModel)]=\"password\">\n              <label for=\"materialFormCardPasswordEx\" class=\"font-weight-light\">password</label>\n            </div>\n            <div class=\"text-center py-4 mt-3\">\n              <button class=\"btn btn-cyan waves-light\" type=\"submit\" mdbWavesEffect (click)='submit()'>LOGIN</button>\n            </div>\n        </div>\n      </div>\n\n      <div mdbModal #basicModal=\"mdbModal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBasicModalLabel\"\n        aria-hidden=\"true\">\n        <div class=\"modal-dialog\" role=\"document\">\n          <div class=\"modal-content\">\n            <div class=\"modal-header\">\n              <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"basicModal.hide()\">\n                <span aria-hidden=\"true\">×</span>\n              </button>\n              <h4 class=\"modal-title w-100\" id=\"myModalLabel\">ERRORS</h4>\n            </div>\n            <div class=\"modal-body\">\n              Please enter username and password\n            </div>\n            <div class=\"modal-footer\">\n              <button type=\"button\" class=\"btn btn-secondary waves-light\" aria-label=\"Close\" (click)=\"basicModal.hide()\" mdbWavesEffect>Close</button>\n            </div>\n          </div>\n        </div>\n      </div>\n\n    </div>\n\n    <div class=\"col\">\n    </div>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoginComponent = /** @class */ (function () {
    function LoginComponent() {
        this.username = '';
        this.password = '';
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.submit = function () {
        if (this.username.trim() == '' || this.password.trim() == '') {
            this.contentModal.show();
        }
        else {
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('basicModal'),
        __metadata("design:type", Object)
    ], LoginComponent.prototype, "contentModal", void 0);
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/pages/pages.component.html":
/*!********************************************!*\
  !*** ./src/app/pages/pages.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"loader\" *ngIf=\"showLoading\">Loading...</div>\n  <br/><br/>\n  <div class=\"row\">\n    <div class=\"col\">\n        <h4>ABOUT US PAGE</h4>\n        <div class=\"md-form\">\n         <textarea style='height:300px;width:100%' [placeholder]=\"'About text here...'\"  [(ngModel)]=\"page.about\"></textarea>\n        </div>\n    </div>\n  </div>\n  <hr/>\n  <div class=\"row\">\n    <div class=\"col\">\n            <h4>BOOK US NOW PAGE</h4>\n        <div class=\"md-form\">\n                <textarea style='height:300px;width:100%' [placeholder]=\"'Book us text here...'\"  [(ngModel)]=\"page.book\"></textarea>\n        </div>\n    </div>\n  </div>\n  <hr/>\n  <div class=\"row\">\n    <div class=\"col\">\n            <h4>ALBUM PAGE</h4>\n        <div class=\"md-form\">\n                <textarea style='height:300px;width:100%' [placeholder]=\"'Album text here...'\" [(ngModel)]=\"page.album\"></textarea>\n        </div>\n    </div>\n  </div>\n  <hr/>\n    <div class=\"row\">\n      <div class=\"col\">\n            <h4>WEDDING PAGE</h4>\n          <div class=\"md-form\">\n                <textarea style='height:300px;width:100%' [placeholder]=\"'Wedding text here...'\" [(ngModel)]=\"page.wedding\"></textarea>\n          </div>\n      </div>\n    </div>\n    <hr/>\n    <div class=\"row\">\n      <div class=\"col\">\n          <div class=\"md-form\">\n              <button type=\"button\" class=\"btn btn-primary waves-light\" mdbWavesEffect (click)=\"updatePages()\">Update</button>\n          </div>\n      </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/pages.component.scss":
/*!********************************************!*\
  !*** ./src/app/pages/pages.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/pages.component.ts":
/*!******************************************!*\
  !*** ./src/app/pages/pages.component.ts ***!
  \******************************************/
/*! exports provided: PagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesComponent", function() { return PagesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PagesComponent = /** @class */ (function () {
    function PagesComponent(afs) {
        var _this = this;
        this.afs = afs;
        this.page = { about: '', book: '', album: '', wedding: '' };
        this.showLoading = true;
        this.itemsCollection = afs.collection('page');
        this.itemDoc = afs.doc('page/EI9bBobft8w6GCLL3s2L');
        this.itemsd = this.itemsCollection.valueChanges();
        this.itemsd.subscribe(function (data) {
            _this.page = data[0];
            _this.showLoading = false;
        });
    }
    PagesComponent.prototype.ngOnInit = function () { };
    PagesComponent.prototype.updatePages = function () {
        this.showLoading = true;
        this.itemDoc.update(this.page);
        var self = this;
        setTimeout(function () {
            self.showLoading = false;
        }, 3000);
    };
    PagesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pages',
            template: __webpack_require__(/*! ./pages.component.html */ "./src/app/pages/pages.component.html"),
            styles: [__webpack_require__(/*! ./pages.component.scss */ "./src/app/pages/pages.component.scss")]
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]])
    ], PagesComponent);
    return PagesComponent;
}());



/***/ }),

/***/ "./src/app/weddings/weddings.component.html":
/*!**************************************************!*\
  !*** ./src/app/weddings/weddings.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class='container'>\n  <div class='row'>\n      <div class=\"card\">\n          <div class=\"card-body\">\n              <div class=\"md-form\">\n                <input mdbInputDirective type=\"text\" id=\"form1\" class=\"form-control\" [(ngModel)]=\"wedEventName\">\n                <label for=\"form1\" class=\"\">Name</label>\n              </div>\n              <div class=\"md-form\">\n                  <input mdbInputDirective type=\"text\" id=\"form1\" class=\"form-control\" [(ngModel)]=\"morelink\">\n                  <label for=\"form1\" class=\"\">More Link</label>\n              </div>\n              <br/>\n              <div>\n                  <button type=\"button\" class=\"btn btn-secondary btn-sm  waves-light\" aria-label=\"Close\" (click)=\"addWedEventName()\" mdbWavesEffect>Save</button>\n              </div>\n          </div>\n      </div>\n  </div>\n  <br/><br/>\n  <div>\n    <table class=\"table\">\n      <tr *ngFor=\"let item of weddingData.weddings; let i = index\">\n        <td>\n          {{item.name}}\n        </td>\n        <td width=\"100px;\">\n          {{item.more}}\n        </td>\n        <td>\n          <div *ngFor=\"let img of item.images; let k = index\" style='overflow:scroll;float:left;'>\n            <div style=\"border:1px solid #eee;\">\n              <img src='{{img}}' style='width:100px;height:100px;'><br/>\n              <button type=\"button\" class=\"btn btn-info btn-sm waves-light\" aria-label=\"Close\" (click)=\"deleteImage(i,k)\" mdbWavesEffect>Delete</button>\n            </div>\n          </div>\n          <div>\n              <button type=\"button\" class=\"btn btn-primary btn-sm  waves-light\" aria-label=\"Close\" (click)=\"selectEvent(i)\" mdbWavesEffect>add image</button>\n          </div>\n          <div>\n              <button type=\"button\" class=\"btn btn-secondary btn-sm  waves-light\" aria-label=\"Close\" (click)=\"deleteWedEvent(i,item)\" mdbWavesEffect>Delete</button>\n          </div>\n        </td>\n      </tr>\n    </table>\n  </div>\n\n  <div mdbModal #sliderModal=\"mdbModal\" class=\"modal fade right\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBasicModalLabel\" aria-hidden=\"true\">\n      <div class=\"modal-dialog modal-side modal-top-right\" role=\"document\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\" style='height:500px;overflow:scroll;'>\n              <div class=\"col-sm-12\" *ngFor=\"let img of imgData.images; index as i;\" style='margin:0px;margin-bottom:20px;border:1px solid #ccc;padding:0px;margin-right:10px;'>\n                  <img src=\"{{img}}\" style='height:200px;object-fit: cover' *ngIf=\"img!=''\" (click)=\"selectSlider(i)\"/>\n              </div>\n          </div>\n          <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-secondary btn-sm  waves-light\" aria-label=\"Close\" (click)=\"sliderModal.hide()\" mdbWavesEffect>Close</button>\n          </div>\n        </div>\n      </div>\n    </div>\n\n  <div mdbModal #loadingModal=\"mdbModal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBasicModalLabel\" aria-hidden=\"true\">\n      <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\">\n            <div class=\"loader\">Loading...</div>\n          </div>\n        </div>\n      </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/weddings/weddings.component.scss":
/*!**************************************************!*\
  !*** ./src/app/weddings/weddings.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/weddings/weddings.component.ts":
/*!************************************************!*\
  !*** ./src/app/weddings/weddings.component.ts ***!
  \************************************************/
/*! exports provided: WeddingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WeddingsComponent", function() { return WeddingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var WeddingsComponent = /** @class */ (function () {
    function WeddingsComponent(afs) {
        var _this = this;
        this.afs = afs;
        this.imgData = { images: [] };
        this.index = 0;
        this.wedEvents = [];
        this.morelink = '';
        this.wedEventName = '';
        this.imageUrl = '';
        this.weddingData = { weddings: [] };
        this.selectedWedEvent = {};
        this.itemsCollection = afs.collection('weddings');
        this.itemDoc = afs.doc('weddings/iyCWnLXI0AvEzj8cyPim');
        this.itemsd = this.itemsCollection.valueChanges();
        this.itemsd.subscribe(function (data) {
            _this.weddingData = data[0];
        });
    }
    WeddingsComponent.prototype.ngOnInit = function () { };
    WeddingsComponent.prototype.addWedEventName = function () {
        if (this.wedEventName != '') {
            this.weddingData.weddings.push({ name: this.wedEventName, more: this.morelink, images: [] });
            this.wedEventName = '';
            this.morelink = '';
            this.updateWedEvent();
        }
    };
    WeddingsComponent.prototype.selectEvent = function (i) {
        var _this = this;
        this.index = i;
        this.imgCollection = this.afs.collection('images');
        this.imgDoc = this.afs.doc('images/PxortbUVj3h5sxHbf5a6');
        this.imgd = this.imgCollection.valueChanges();
        this.imgd.subscribe(function (data) {
            _this.imgData = data[0];
            _this.sliderModal.show();
        });
    };
    WeddingsComponent.prototype.deleteImage = function (i, k) {
        this.weddingData.weddings[i].images.splice(k, 1);
        this.updateWedEvent();
    };
    WeddingsComponent.prototype.deleteWedEvent = function (i) {
        this.weddingData.weddings.splice(i, 1);
        this.updateWedEvent();
    };
    WeddingsComponent.prototype.selectSlider = function (index) {
        this.sliderModal.hide();
        this.weddingData.weddings[this.index].images.push(this.imgData.images[index]);
        this.updateWedEvent();
    };
    WeddingsComponent.prototype.updateWedEvent = function () {
        console.log(this.weddingData);
        this.loadingModal.show();
        this.itemDoc.update(this.weddingData);
        var self = this;
        setTimeout(function () {
            self.loadingModal.hide();
        }, 3000);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('loadingModal'),
        __metadata("design:type", Object)
    ], WeddingsComponent.prototype, "loadingModal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sliderModal'),
        __metadata("design:type", Object)
    ], WeddingsComponent.prototype, "sliderModal", void 0);
    WeddingsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-weddings',
            template: __webpack_require__(/*! ./weddings.component.html */ "./src/app/weddings/weddings.component.html"),
            styles: [__webpack_require__(/*! ./weddings.component.scss */ "./src/app/weddings/weddings.component.scss")]
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]])
    ], WeddingsComponent);
    return WeddingsComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyBgz8jLeK34MniB0h7w-Zm08JY4yWgtsTM",
        authDomain: "yeluguri-35a53.firebaseapp.com",
        databaseURL: "https://yeluguri-35a53.firebaseio.com",
        projectId: "yeluguri-35a53",
        storageBucket: "yeluguri-35a53.appspot.com",
        messagingSenderId: "595073759408"
    }
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
document.addEventListener('DOMContentLoaded', function () {
    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
        .catch(function (err) { return console.log(err); });
});


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/sanjith/Desktop/yeluguri/dynamic/yeluguri-admin/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map